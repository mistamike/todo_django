from django.http import HttpResponse
from django.http import JsonResponse
from Django_db.models import Todo
from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder
import json


def index(request):
    return render(request, "index.html")


# def func_qwe(request):
#     if request.method == "POST":
#         func_addTodo(request)
#     elif request.method == "GET":
#         print('ssssss')
#     return HttpResponse('privet')


def func_addTodo(request):
    if request.method == "POST":
        list_element = json.loads(request.body.decode('UTF-8'))
        status = list_element.get('done')
        add_to_base = Todo(id=list_element.get('id'),
                           text=list_element.get('text'),
                           done=status)
        add_to_base.save()
        sas = Todo.objects.filter(id=list_element.get('id'))
        todo_list = sas.values('id')
        todo_list = json.dumps(list(todo_list), cls=DjangoJSONEncoder)
        return HttpResponse(todo_list)


def func_delete_all(request):
    if request.method == "DELETE":
        Todo.objects.all().delete()
        deleted_all_elements = True
        return HttpResponse(deleted_all_elements)


def func_get_all(request):
    if request.method == "GET":
        all_db_elements = Todo.objects.all().values()
        all_db_elements = list(all_db_elements)
        return JsonResponse(all_db_elements, safe=False)


def func_delete_element(request, id):
    if request.method == "DELETE":
        deleting_element = Todo.objects.filter(id=int(id)).delete()
        element_is_deleted = True
        return HttpResponse(json.dumps(element_is_deleted))


def func_toggling_status(request, id):
    if request.method == "POST":
        list_element = request.POST.get
        element_id = int(list_element('id'))
        element_is_done = list_element('status')
        get_element = Todo.objects.get(id=element_id)
        if (element_is_done == 'false'):
            get_element.done = True
            get_element.save()
            return HttpResponse(json.dumps(get_element.done))
        else:
            get_element.done = False
            get_element.save()
            return HttpResponse(json.dumps(get_element.done))


def func_edit_text(request, id):
    if request.method == "POST":
        list_element = request.POST.get
        element_id = int(list_element('id'))
        element_new_text = list_element('newText')
        get_element = Todo.objects.get(id=element_id)
        get_element.text = element_new_text
        get_element.save()
        element_changed = True
    return HttpResponse(json.dumps(element_changed))


def func_delete_finished(request):
    if request.method == "DELETE":
      objects = Todo.objects.filter(done=True)
      objects.delete()
      finished_are_deleted = True
    return HttpResponse(finished_are_deleted)


def func_check_uncheck_all(request):
    if request.method == "POST":
        requested_elements = json.loads(request.body.decode('UTF-8'))
        # requested_elements = request.POST.get('is_checked')
        if requested_elements['is_checked'] == True:
            requested_status = True
        else:
            requested_status = False
        objects = Todo.objects.all()
        for model_element in objects:
            if requested_status == True:
                model_element.done = True
                model_element.save()
            else:
                model_element.done = False
                model_element.save()
        return HttpResponse(model_element.done)

"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from .views import index, func_addTodo, func_delete_all, func_get_all,\
func_delete_element, func_toggling_status, func_edit_text, func_delete_finished, \
func_check_uncheck_all
admin.autodiscover()


urlpatterns = [
    url(r'admin/', admin.site.urls),
    url(r'add_todo', func_addTodo),
    url(r'delete_all', func_delete_all),
    url(r'delete_finished', func_delete_finished),
    url(r'check_uncheck_all', func_check_uncheck_all),
    url(r'^delete_single_element/(?P<id>\d+)/$', func_delete_element),
    url(r'^toggle_class_element/(?P<id>\d+)/$', func_toggling_status),
    url(r'^edit_element/(?P<id>\d+)/$', func_edit_text),
    url(r'get_all', func_get_all),
    url(r'', index),



]

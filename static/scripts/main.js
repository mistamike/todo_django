$(function () {
    /* jshint validthis: true */
    const inputSection = $('.main-section__input'); // Main input section
    const todoList = $('#todo-list'); // Unsorted list of elements
    const elementsPerPage = 5; // Elements per page
    let checkAll = $('.choose-all');
    let itemId = 0; // Initial element ID
    let finishedCounter = 0; // Counter of finished to-do elements
    let unfinishedCounter = 0; // Counter of unfinished to-do elements
    let allElementsArr = []; // Array of to-do elements
    let showArr = []; // Array to display elements per page
    let currentPage = 1; // Current page on start
    let numOfPages = 1; // Quantity of pages on start
    let isCall = true; // Value for blur 'event'
    let notFinishedText = $('.not-finished');
    let finishedText = $('.finished');

    //#region Call of functions

    ajaxGetAllElements(); //Get all elements from database

    // Removing list element
    todoList.on('click', '.clear-todo', removeTodo);

    // Clear all list elements
    $('.clear-all').on('click', clearAll);

    // Toggling class 'done' if checkbox is checked/unchecked
    todoList.on('click', '.checkElement', doneUndone);

    // Choose/unchoose all elements
    checkAll.on('click', ajaxCheckUncheckAll);

    // Check radiobuttons
    $('.radioButton').on('click', radioChecked);

    // Add new list element by click and clear input area
    $('#main-section__form').on('submit', function (e) {
        e.preventDefault();
        if (inputSection.val().trim() === "") { // The case when nothing entered
            return inputSection.val('');
        } else {
            // Add new element
            todoList.append(`
                    <li class="list-element">
                        <input class="checkElement" type="checkbox">
                            <div class = "list-element__content">
                                ${inputSection.val().trim()}
                            </div>
                        <button class="clear-todo" type="button">X</button>
                    </li>
            `);
        }
        // Add id for new element
        $('.list-element').last().attr('id', 'id' + itemId); // Add id for new element
        let ajaxTodoObject = {id: itemId, text: inputSection.val(), done: false};
        addTodo(ajaxTodoObject);
    });

    // Edit single element by double click
    todoList.on('dblclick', '.list-element__content', editElement);

    // Apply edited element (by double click)
    todoList.on('keydown', '.edit', applyEditedElement);

    // Toggle class 'currentPage'
    $('#navigation').on('click', '.page', selectCurrentPage);

    // Cancel editing element
    todoList.on('blur', '.edit', function () {
        let inputVal = $('.edit');
        if (isCall) {
            Blurhandler(inputVal);
        } else {
            isCall = false;
        }
        isCall = true;
    });

    // Delete all finished elements
    $('.actionButtons').on('click', '.delete-finished', ajaxDeleteFinished);

    //#endregion Call of functions


    //#region Functions description
    // Removing list element function
    function removeTodo() {
        let id = $(this).parent().attr('id');
        let index = findIndexById(id);
        if ($(this).parent().hasClass('done')) { // Decrease counter for done/undone element
            finishedCounter--;
        } else {
            unfinishedCounter--;
        }
        $(this).parent().remove();
        ajaxDeleteElement(allElementsArr[index].id, index);
    }


    //Clear all list elements function
    function clearAll() {
        // let messageToUser = confirm("All your tasks will be removed. Are you sure?");
        // if (messageToUser) {
        ajaxDeleteAll();
        // let checkAll = $('.choose-all');
        finishedCounter = 0;
        unfinishedCounter = 0;
        $('.list-element').remove();
        allElementsArr = [];
        checkAll.removeClass("unchoose-all");
        checkAll.text('Finish all');
        updateCountersView();
        showHideElements();
        // } else {
        //     return alert("Something went wrong and your elements can't be removed");
        // }

    }

    //Toggling class 'done' if checkbox is checked/unchecked function
    function doneUndone() {
        let id = $(this).parent().attr('id');
        let index = findIndexById(id);
        let currentElement = $(this).parent();
        ajaxToggleStatus(id, currentElement, index);
    }


    //Count done/undone elements function
    function updateCountersView() {
        notFinishedText.text('Not finished: ' + unfinishedCounter);
        finishedText.text('Finished: ' + finishedCounter);
    }

    //Check radiobuttons function
    function radioChecked() {
        let radioButton = $('.radioButton');
        radioButton.change(function () {
            radioButton.removeClass('checkedButton');
            $(this).addClass('checkedButton');
            showHideElements();
        });
    }

    //Show and hide elements function (all/active/completed)
    function showHideElements(last = false) {
        if ($('#radio1').hasClass('checkedButton')) { // 'All' chosen
            renderPage(last);
        } else if ($('#radio2').hasClass('checkedButton')) { // 'Active' chosen
            renderPage(last, {done: false});
        } else if ($('#radio3').hasClass('checkedButton')) { // 'Completed' chosen
            renderPage(last, {done: true});
        }
    }

    // Find index of array by id function
    function findIndexById(id) {
        let index = null;
        allElementsArr.forEach(function (el, idx) {
            if (el.id === id) {
                index = idx;
            }
        });
        return index;
    }

    // Toggle status (done/undone) for 'Finish all' button function
    function toggleArrStatus(isChecked) {
        for (let i = 0; i < allElementsArr.length; i++) {
            allElementsArr[i].done = isChecked;
        }
    }

    // Edit single element by double click function
    function editElement() {
        let editableContent = $(this);
        let textArea = $('<input class="edit" type="text">');
        editableContent.replaceWith(textArea); // Replace element text with input area
        textArea.val(editableContent.text().trim()).focus(); // Focus on created input
    }

    // Apply edited element (by double click) function
    function applyEditedElement(e) {
        let edit = $('.edit');
        let id = $(this).parent().attr('id');
        let index = findIndexById(id); // Find element by ID
        let originalText = allElementsArr[index].text;
        // Accept changes by "Enter" button
        if (e.keyCode === 13) {
            if (edit.val().trim() === "" || edit.val() === originalText) {
                return false;
            } else {
                isCall = false;
                let replacingValue = $(this);
                ajaxEditElement(id, replacingValue, edit, index);
            }
            // Cancel changes by "Esc" button
        } else if (e.keyCode === 27) {
            isCall = false;
            $(this).replaceWith($('<div class = "list-element__content">' + originalText + '</div>'));
        }

    }

    // Recount pages per ALL list elements function
    function recountPages(renderArr) {
        let numPages;
        numPages = Math.ceil(renderArr.length / elementsPerPage);
        return numPages;
    }

    // Render pages function
    function renderPage(last, renderFlag) {
        let renderArr = []; // New array for rendering pages
        let navigation = $('#navigation');
        if (!renderFlag) {
            renderArr = allElementsArr;
        }
        else {
            renderArr = allElementsArr.filter(function (el) {
                return el.done === renderFlag.done;
            });
        }
        let numOfPagesNew = recountPages(renderArr);
        $('.page').remove();
        // Save first page if we have before deleting one element
        // or in case of total removing
        if (numOfPagesNew === 0) {
            numOfPagesNew++;
        }
        if (currentPage > numOfPagesNew || last) {
            currentPage = numOfPagesNew;
        }
        for (let i = 1; i <= numOfPagesNew; i++) {
            const activeClass = (i === currentPage) ? 'currentPage' : '';
            navigation.append(`<a href="#" class="page ${activeClass}" id="pageId ${i}">${i} </a>`);

        }
        $('.main-section__input').focus();
        showArr = countPagesPerElements(currentPage, renderArr);
        $('.list-element').hide();
        renderFromArray(showArr);
    }

    // Toggle class 'currentPage' function
    function selectCurrentPage(e) {
        e.preventDefault();
        let renderArr = []; // New array for rendering pages
        // Render pages according to the buttons ("All"/"Active/"Completed")
        if ($('#radio1').hasClass('checkedButton')) {
            renderArr = allElementsArr;
        } else if ($('#radio2').hasClass('checkedButton')) {
            renderArr = allElementsArr.filter(function (el) {
                return !el.done;
            });
        } else if ($('#radio3').hasClass('checkedButton')) {
            renderArr = allElementsArr.filter(function (el) {
                return el.done;
            });
        } else {
            renderArr = allElementsArr;
        }
        $('.page').removeClass('currentPage'); // Remove 'currentPage' status from all pages
        $(this).addClass('currentPage'); // Add 'currentPage' status to chosen page
        currentPage = +($(this).attr('id').replace('pageId', ''));
        $('.main-section__input').focus();
        showArr = countPagesPerElements(currentPage, renderArr);
        $('.list-element').hide();
        renderFromArray(showArr);
    }

    // Show pages from "showArr" array function
    function renderFromArray(list) {
        list.forEach(function (el, idx) {
            $('#' + el.id).show();
        });
    }

    // Show/hide "delete all finished" button function
    function showDeleteFinishedButton() {
        let deleteFinishedButton = $('.delete-finished');
        if ($('.done').length) {
            deleteFinishedButton.show();
        } else {
            deleteFinishedButton.hide();
        }
    }

    // 'Blur' event handler function
    function Blurhandler(inputVal) {
        let id = inputVal.parent().attr('id');
        let index = findIndexById(id);
        let originalText = allElementsArr[index].text;
        inputVal.replaceWith($('<div class = "list-element__content">' + originalText + '</div>'));
    }

    //Count pages for elements
    function countPagesPerElements(currentPage, arrayForRendering) {
        let startRender = (currentPage - 1) * elementsPerPage; // Starting position with which the first element for the current page will start
        let endRender = startRender + elementsPerPage; // The final position at which the countdown for the current page will end
        showArr = arrayForRendering.slice(startRender, endRender); // Add in new array "showArr" elements which match their pages
        return showArr;
    }

    //#endregion Functions description

    //
    // function qwe(text) {
    //     $.ajax({
    //         url: 'qwe/',
    //         type: "POST",
    //         data: text,
    //         success: function (data) {
    //             alert(data)
    //         },
    //         error: function () {
    //             alert('Bad!')
    //         }
    //     });
    // }

    function addTodo(element) {
        // let checkAll = $('.choose-all');
        let isChecked = false;
        if (checkAll.hasClass('unchoose-all')) {
            checkAll.removeClass('unchoose-all');
        }
        $.ajax({
            url: '/add_todo/',
            type: "POST",
            data: JSON.stringify(element),
            success: function (response) {
                let djngResponsedId = JSON.parse(response);
                allElementsArr.push({id: 'id' + djngResponsedId[0].id, text: inputSection.val(), done: false}); // Add element in 'allElementsArr' array
                itemId++;
                unfinishedCounter++; // Gain unfinished elements counter
                inputSection.val(''); // Clear input section
                checkAll.removeClass("unchoose-all");
                checkAll.text(isChecked ? 'Cancel all' : 'Finish all'); // Rename button
                updateCountersView();
                showHideElements(true);
                showDeleteFinishedButton();
            }
            // error: function (error) {
            //     console.log("Error getting data from the server:", error);
            // }
        });
    }

    function ajaxDeleteAll() {
        $.ajax({
            url: '/delete_all/',
            type: "DELETE",
            success: function (response) {
                if (response === 'True') {
                    // let checkAll = $('.choose-all');
                    finishedCounter = 0;
                    unfinishedCounter = 0;
                    $('.list-element').remove();
                    allElementsArr = [];
                    checkAll.removeClass("unchoose-all");
                    checkAll.text('Finish all');
                    updateCountersView();
                    showHideElements();
                    itemId = 0;
                }
            }
            // error: function () {
            //     alert('not deleted');
            // }
        });
    }

    function ajaxGetAllElements() {
        // let checkAll = $('.choose-all');
        let isChecked = !checkAll.hasClass('unchoose-all');
        let checkElement = $('.checkElement');
        $.ajax({
            url: '/get_all/',
            type: "GET",
            success: function (response) {
                let djngReceivedElement = response.sort(function sortById(firstEl, secondEl) {
                    return firstEl.id - secondEl.id;
                });
                for (let i = 0; i < djngReceivedElement.length; i++) {
                    if (djngReceivedElement[i].done === true) {
                        todoList.append(`
                            <li class="list-element done">
                            <input class="checkElement" type="checkbox" checked>
                            <div class = "list-element__content">
                            ${djngReceivedElement[i].text}
                            </div>
                            <button class="clear-todo" type="button">X</button>
                            </li>
                        `);
                        finishedCounter++;
                    } else {
                        todoList.append(`
                            <li class="list-element">
                            <input class="checkElement" type="checkbox">
                            <div class = "list-element__content">
                            ${djngReceivedElement[i].text}
                            </div>
                            <button class="clear-todo" type="button">X</button>
                            </li>
                        `);
                        unfinishedCounter++;
                    }
                    $('.list-element').last().attr('id', 'id' + djngReceivedElement[i].id); // Add id for new element
                    allElementsArr.push({
                        id: 'id' + djngReceivedElement[i].id,
                        text: djngReceivedElement[i].text,
                        done: djngReceivedElement[i].done
                    }); // Add element in 'allElementsArr' array

                }

                if ($('.list-element').length === $('.done').length) {
                    isChecked = false;
                    toggleArrStatus(isChecked); // Toggle status (done/undone) for 'Finish all' button
                    checkElement.prop('checked', isChecked);
                    checkAll.toggleClass("unchoose-all");
                } else {
                    isChecked = true;
                    checkElement.prop('checked', isChecked);
                }
                checkAll.text(isChecked ? 'Finish all' : 'Cancel all'); // Rename button

                if (allElementsArr.length !== 0) {
                    showDeleteFinishedButton();
                    itemId = djngReceivedElement[djngReceivedElement.length - 1].id;
                    itemId++;
                } else {
                    itemId = 0;
                }
                updateCountersView();
                showHideElements();
                showDeleteFinishedButton();
            }
            // error: function () {
            //     alert('not added')
            // }
        });
    }

    function ajaxDeleteElement(id, elementIndex) {
        id = +(id.substr(2));
        $.ajax({
            url: `delete_single_element/${id}/`,
            type: "DELETE",
            data: {'id': id},
            success: function (response) {
                if (response) {
                    allElementsArr.splice(elementIndex, 1); // Deleting element from array
                    updateCountersView();
                    showHideElements();
                }
            }
            // error: function (error) {
            //     alert('Ur element is NOT deleted')
            // }
        });

    }

    function ajaxToggleStatus(id, element, elementIndex) {
        // let checkAll = $('.choose-all');
        let isChecked = !checkAll.hasClass('unchoose-all');
        id = +(id.substr(2));
        $.ajax({
            url: `toggle_class_element/${id}/`,
            type: "POST",
            data: {
                'id': id,
                'status': element.hasClass('done')
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response === true) {
                    finishedCounter++;
                    unfinishedCounter--;
                    element.toggleClass('done');
                    allElementsArr[elementIndex].done = true;
                } else {
                    finishedCounter--;
                    unfinishedCounter++;
                    element.toggleClass('done');
                    allElementsArr[elementIndex].done = false;
                }
                if ($('.list-element').length === $('.done').length) {
                    isChecked = true;
                    checkAll.addClass("unchoose-all");
                } else {
                    isChecked = false;
                    checkAll.removeClass("unchoose-all");
                }
                checkAll.text(isChecked ? 'Cancel all' : 'Finish all'); // Rename button
                showDeleteFinishedButton();
                updateCountersView();
                showHideElements();
            }
            // error: function (error) {
            //     alert('Not toggled')
            // }
        });
    }

    function ajaxEditElement(id, newText, editValue, elementIndex) {
        id = +(id.substr(2));
        $.ajax({
            url: `edit_element/${id}/`,
            type: "POST",
            data: {
                'id': id,
                'newText': editValue.val()
            },
            success: function (response) {
                response = JSON.parse(response);
                if (response === true) {
                    newText.replaceWith($(`<div class = "list-element__content">${editValue.val().trim()}</div>`));
                    allElementsArr[elementIndex].text = editValue.val();
                    // } else {
                    //     console.log('text was NOT changed');
                }
                showDeleteFinishedButton();
                updateCountersView();
                showHideElements();
            }
            // error: function (error) {
            //     alert('Not toggled')
            // }
        });
    }

    // Delete all finished elements function
    function ajaxDeleteFinished() {
        $.ajax({
            url: 'delete_finished',
            type: 'DELETE',
            success: function () {
                $('.done').remove();
                let undoneArr = allElementsArr.filter(function (el) {
                    return !el.done;
                });
                allElementsArr = undoneArr;
                showDeleteFinishedButton();
                finishedCounter = 0;
                updateCountersView();
                showHideElements();
            }
            // error: function () {
            //     alert('not deleted')
            // }
        });
    }

    function ajaxCheckUncheckAll() {
        // let checkAll = $('.choose-all');
        let isChecked = !checkAll.hasClass('unchoose-all');
        let checkElement = $('.checkElement');
        $.ajax({
            url: 'check_uncheck_all',
            type: "POST",
            data: JSON.stringify({
                'is_checked': isChecked
            }),
            success: function () {
                if (isChecked) {
                    finishedCounter = finishedCounter + unfinishedCounter;
                    unfinishedCounter = 0;
                    checkElement.attr('checked');
                    $('.list-element').addClass('done');
                } else {
                    unfinishedCounter = unfinishedCounter + finishedCounter;
                    finishedCounter = 0;
                    checkElement.removeAttr('checked');
                    $('.list-element').removeClass('done');
                }
                toggleArrStatus(isChecked); // Toggle status (done/undone) for 'Cancel all' button
                showDeleteFinishedButton();
                checkElement.prop('checked', isChecked);
                checkAll.toggleClass("unchoose-all");
                checkAll.text(isChecked ? 'Cancel all' : 'Finish all'); // Rename button
                updateCountersView();
                showHideElements();
            }
            // error: function () {
            //     alert('not checked')
            // }
        });
    }


});
